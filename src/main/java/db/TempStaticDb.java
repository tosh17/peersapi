package db;

import dataunit.Men;

import java.util.ArrayList;
import java.util.List;

import static dataunit.Men.Sex.*;

public class TempStaticDb {
    List<Men>list = new ArrayList();
    public TempStaticDb() {
        init();
    }

    private void init() {
        list.add(new Men(1,"Petr",M,25));
        list.add(new Men(2,"Oleg",M,43));
        list.add(new Men(3,"Mike",M,25));
        list.add(new Men(4,"Anton",M,53));
        list.add(new Men(5,"Zak",M,67));
        list.add(new Men(6,"Olga",F,23));
        list.add(new Men(7,"Zina",F,82));
        list.add(new Men(8,"Rita",F,63));
        list.add(new Men(9,"Kate",F,12));
        list.add(new Men(10,"PetrWoman",F,32));
        list.add(new Men(11,"42ya",F,45));
        list.add(new Men(12,"Lego",M,17));
        list.add(new Men(13,"Lisa",F,67));
        list.add(new Men(14,"Petros",M,98));
        list.add(new Men(15,"Q5",M,123));
   }

   public Men getUserById(int id){
        return list.get(id);
   }
   public List<Men> getUserBeetwenAge(int age1,int age2){
        List<Men> tempList=new ArrayList<>();
        for(Men user:list){
            if(user.getAge()>=age1 && user.getAge()<=age2 ) tempList.add(user);
        }
        return tempList;
   }
   public  List<Men> getAll(){
        return list;
   }
}
