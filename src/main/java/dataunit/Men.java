package dataunit;

public class Men {
    int id;
    int age;
    String name;
    Sex sex;

    public Men(int id, String name, Sex sex, int age) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public Men setId(int id) {
        this.id = id;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Men setAge(int age) {
        this.age = age;
        return this;
    }

    public Sex getSex() {
        return sex;
    }

    public Men setSex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public enum Sex {
        M, F;
    }
}
